import React, { Component } from 'react';
import './post.sass';
import logo from '../logo.svg';
import { Button, Icon, Label, Divider, Dropdown, Modal } from 'semantic-ui-react';

class Post extends Component {
    constructor(props) {
        super(props);

        this.state = {
            likes: 0,
            shares: 0
        }
    }

    componentDidMount = () => {
        this.setState({
            likes: this.props.postData.likes,
            shares: this.props.postData.shares
        })
    }

    render() {
        return (
        <div className="post">
            <div className="card">
                <div className="header">
                    <img src={logo} width="50px" height="50px" alt="User logo"/>
                    <div className="post-info">
                        <div className="author-name">{this.props.postData.authorName}</div>
                        <div className="date">{this.props.postData.date}</div>
                    </div>
                </div>
                <div className="body">
                    {this.props.postData.text}
                </div>
                <Divider />
                <div className="footer">
                    <Button as='div' labelPosition='right'>
                        <Button icon onClick={this.likePost}>
                            <Icon name='heart' />
                            Like
                        </Button>
                        <Label as='a' basic pointing='left'>
                            {this.state.likes}
                        </Label>
                    </Button>
                    <Button as='div' labelPosition='right'>
                        <Button icon onClick={this.sharePost}>
                            <Icon name='fork' />
                            Share
                        </Button>
                        <Label as='a' basic pointing='left'>
                            {this.state.shares}
                        </Label>
                    </Button>
                    <Dropdown button text='Options'>
                        <Dropdown.Menu>
                            <Dropdown.Item text='Delete' />
                            <Dropdown.Item text='Report' />                            
                        </Dropdown.Menu>
                    </Dropdown>
                </div>
            </div>
            {/* <Modal size={size} open={open} onClose={this.close}>
                <Modal.Header>Delete Your Account</Modal.Header>
                <Modal.Content>
                <p>Are you sure you want to delete your account</p>
                </Modal.Content>
                <Modal.Actions>
                <Button negative>No</Button>
                <Button positive icon='checkmark' labelPosition='right' content='Yes' />
                </Modal.Actions>
            </Modal> */}
        </div>)
    }

    likePost = () => {
        this.setState({
            likes: this.state.likes + 1
        });
    }

    sharePost = () => {
        this.setState({
            shares: this.state.shares + 1
        });
    }

}

export default Post;