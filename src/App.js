import React, { Component } from 'react';
import 'semantic-ui-css/semantic.min.css';
import './App.sass';
import Post from './post/post';

class App extends Component {
  post1 = {
    authorName: 'Nikita Krasnov',
    date: '20 Apr 2019 13:41',
    likes: 2048,
    shares: 512,
    text: 'This is my first post in Twitterize.'
  }

  post2 = {
    authorName: 'Marie Simkiv',
    date: '20 Apr 2019 14:32',
    likes: 348,
    shares: 102,
    text: 'Cooking pizza...'
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          Twitterize
        </header>
        <Post postData={this.post1}/>
        <Post postData={this.post2}/>
      </div>
    );
  }
}

export default App;
